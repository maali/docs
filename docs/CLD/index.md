# Detector Dimensions

Put detector dimensions

# Recipe (ddsim + MarlinWrappers)

## Description of the output rootfile content
| Collection name | Content | 
|---|---|
| _SiTracks_Refitted_trackStates | TrackState at 4 different points: the IP, the first hit, the last hit and the calorimeter. To know which TrackState correspond to e.g. the one at the IP, use something like myTrackState.location == edm4hep::TrackState::AtIP;  (see [here](https://github.com/key4hep/EDM4hep/blob/main/edm4hep.yaml#L84C26-L84C30)).   |
# FCC-ee Full Sim Webpage

Welcome to the FCC-ee Full Sim webpage! This page hosts relevant information regarding FCC-ee detectors implementation, simulation and reconstruction in the Key4hep framework. It includes recipes to run the various simulations, detector dimensions, name of contact persons and much more!

## General Considerations

The goal we are pursuing is to implement all the FCC-ee subdetector geometries and reconstruction in a consistent software framework ([Key4hep](https://key4hep.github.io/key4hep-doc/)) to, among other benefits, be able to easily exchange part of the simulation accross detector concepts.

Recent progress, plans, faced difficulties, etc, are discussed at the bi-weekly [FCC Detector Full Sim working meeting](https://indico.cern.ch/category/16938/).

## Using Key4hep

Key4hep is a stack of software packages useful in HEP covering a wide range of applications (event generation, reconstruction, vizualisation, ...). The Key4hep team is taking care of building these packages in a consistent way and deliver on a regular basis the so-called "stable stack" releases. To access all these packages from your terminal session, it is enough to call:

`source /cvmfs/sw.hsf.org/key4hep/setup.sh`

NB: to know what packages are available in the Key4hep environment, which is based on the [Spack](https://spack.io/) package manager but has its own fork for agility, take a look at the [Key4hep-spack](https://github.com/key4hep/key4hep-spack/tree/main/packages) and [Spack](https://github.com/spack/spack/tree/develop/var/spack/repos/builtin/packages) repositories. 

Sometimes, we rely on a feature which was developped after the stable stack was released. For this, there is a mechanism that re-builds a complete stack every night by using the `master/main` branch of each git repository included in Key4hep. The so-called 'nightly build' can be accessed with:

`source /cvmfs/sw-nightlies.hsf.org/key4hep/setup.sh`

Mind that this environment does not guarantee stability, some things might be broken due to ever evolving components relying on each other. To list all the 'nigthlies' available, for instane to re-use one that was working for your purpose, use:

`ls -lah /cvmfs/sw-nightlies.hsf.org/key4hep/releases`

## Developing in Key4hep

Soon you will have the need to develop new features. To do so there are a few useful things to know:

1. Once a given environment has been sourced, everything is taken from there. If you want to apply modifications to e.g. a detector in [k4geo](https://github.com/key4hep/k4geo), clone it locally (in a public folder so that it is easier for the support team to help you in debugging), apply your changes and build it with e.g.

```
mkdir build install
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=../install
make install -j 8
```
 So far, this won't change the behavior of any application since everything is still taken from the central k4geo in Key4hep. In order to pick-up your updated version instead of the central one you have to modify a few environment variables. The list of variables to modify depends on the package but calling the following in the folder hosting the modified repository is usually enough:

 `export K4GEO=$PWD/k4geo/;PATH=$PWD/k4geo/install/bin/:$PATH;CMAKE_PREFIX_PATH=$PWD/k4geo/install/:$CMAKE_PREFIX_PATH;LD_LIBRARY_PATH=$PWD/k4geo/install/lib:$PWD/k4geo/install/lib64:$LD_LIBRARY_PATH;export PYTHONPATH=$PWD/k4geo/install/python:$PYTHONPATH`
#CHECK!!

Part of the above command may not be needed in all cases but adding dummy paths to these environment variables is generally harmless so you can safely use it also for other packages by changing k4geo to e.g. k4SimGeant4.

2. When a given Gaudi Configurables fails, it is not always easy to know which repository hosts it. This command will help you finding this infromation:

`k4run -l` lists all the configurables and their orignal location. The list being quite long, you may want to pipe it to grep. For instance, if you want to know where `GeoSvc` is living call:

`k4run -l | grep -A 1 GeoSvc`

3. Developing in a large framework which is used by many people requires some discipline to keep consistency accross the various components. Try to write code as clean as you can, document it as much as you can with README's and with inline comments (your present and future collaborators will thank you for that), add tests for newly introduced features and run `clang-format` for syntax.